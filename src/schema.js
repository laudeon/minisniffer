import mongoose from 'mongoose'

const AirqualitySchema = new mongoose.Schema({
  tvoc: {
    type: Number,
    require: true
  },
  eco2: {
    type: Number,
    require: true
  }
}, {
  collection: 'airquality',
  timestamps: true
})

export default AirqualitySchema