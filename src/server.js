import dotenv from 'dotenv'
dotenv.config({ silent: process.env.NODE_ENV === 'production' })

import cluster from 'cluster'
import os from 'os'
import mongoose from 'mongoose'

import app from './app'

const numCPUs = os.cpus().length
const port = process.env.PORT || 3000

if (cluster.isMaster) {
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork()
  }

  cluster.on('exit', (worker, code, signal) => {
    cluster.fork()
  })
} else {

  mongoose.connect(`mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DBNAME}`, {
    autoIndex: (process.env.NODE_ENV === 'development'),
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASS,
    useNewUrlParser: true
  })
    .then(() => console.log('mongoose connected successfuly to the mongodb mlab database'))
    .catch(console.log)
  
  app.listen(port)
  
  process.on('uncaughtException', err => {
    console.log(err)
    process.exit(1)
  })
}

export default app
