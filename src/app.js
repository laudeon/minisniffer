import express from 'express'
import helmet from 'helmet'
import cors from 'cors'
import bodyparser from 'body-parser'
import mongoose from 'mongoose'

import schema from './schema'

const app = express()
const Airquality = mongoose.model('Airquality', schema)

app.use(helmet())
app.use(cors())

app.use(bodyparser.json())
app.use(bodyparser.urlencoded({ extended: true }))

// GET all entries
app.get('/v1/airquality', (req, res, next) => {
  Airquality.find()
    .then(data => {
      res.json(data)
    })
    .catch(next)
})

// POST a new entry and get all of them back
// TODO add stats
app.post('/v1/airquality', (req, res, next) => {
  const airquality = new Airquality(req.body)
  const date = new Date()
  const daysToDeletion = 7
  const deletionDate = new Date(date.setDate(date.getDate() - daysToDeletion))

  airquality.save()
    .then(() => Airquality.deleteMany({ _created_at: {$lt: deletionDate } }))
    .then(() => res.status(201).json(airquality))
    .catch(next)
})


// Errors handling
app.use((err, req, res, next) => {
  console.log(err)
  res.status(500).json({ msg: 'oops, error' })
})

export default app
