#include <Wire.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include "Adafruit_SGP30.h"

Adafruit_SGP30 sgp;
WiFiClientSecure client;

const char* ssid = "Livebox-9F0A";
const char* password = "C9C700533F04E0E75F1EF7270A";

const int httpsPort = 443;
const char* host = "minisniffer.herokuapp.com";
const char* fingerprint = "08:3B:71:72:02:43:6E:CA:ED:42:86:93:BA:7E:DF:81:C4:BC:62:30";

int counterBaseline = 0;
int counterToSend = 0;
uint16_t TVOC_base, eCO2_base;
int sum_TVOC = 0;
int sum_ECO2 = 0;

// Tests the https connection to the given host and port
// Prints to the serial the result of the test
void testConnectionTo(const char* host, int httpsPort) {
  Serial.print("connecting to ");
  Serial.println(host);

  if (!client.connect(host, httpsPort)) {
    Serial.println("connection failed");
    return;
  }

  if (client.verify(fingerprint, host)) {
    Serial.println("certificate matches");
  } else {
    Serial.println("certificate doesn't match");
  }
}

// Connects and sends the gas sensor data to the given host
void sendData(int eco2, int tvoc) {
  testConnectionTo(host, httpsPort);

  // Final data string: "eco2=?&tvoc=?"
  String PostData = "eco2=";
  PostData = PostData + eco2;
  PostData = PostData + "&tvoc=";
  PostData = PostData + tvoc;

  // Create a POST https request
  // Send the request
  client.println("POST /v1/airquality HTTP/1.1");
  client.println("Host: minisniffer.herokuapp.com");
  client.println("Cache-Control: no-cache");
  client.println("Content-Type: application/x-www-form-urlencoded");
  client.print("Content-Length: ");
  client.println(PostData.length());
  client.println();
  client.println(PostData);
  Serial.println("request sent");

  // handle the API http response
  // First read the http headers
  // Then, read the body response
  while (client.connected()) {
    String line = client.readStringUntil('\n');
    if (line == "\r") {
      Serial.println("headers received");
      break;
    }
  }
  String line = client.readStringUntil('\n');
  Serial.println("reply was:");
  Serial.println(line);
}

// Automatically executed on the chip boot
// Establishes the Wifi connection to the given router
// Starts the communication with the gas sensor SGP30
void setup() {
  // Begins the com between the chip and the computer
  Serial.begin(115200);

  // Wifi connection
  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  delay(500);

  // Sensor com
  Serial.println("SGP30 test");
  if (! sgp.begin()){
    Serial.println("Sensor not found :(");
    while (1);
  }
  Serial.print("Found SGP30");
}

// Executed as loop, indefinitely.
// get SGP30 data, send them to the API
void loop() {
  if (! sgp.IAQmeasure()) {
    Serial.println("Measurement failed");
    return;
  }

  sum_TVOC = sum_TVOC + sgp.TVOC;
  sum_ECO2 = sum_ECO2 + sgp.eCO2;
  
  Serial.print("TVOC "); Serial.print(sgp.TVOC); Serial.print(" ppb\t");
  Serial.print("eCO2 "); Serial.print(sgp.eCO2); Serial.println(" ppm");

  delay(1000);

  counterBaseline++;
  counterToSend++;

  // Every 30 seconds
  // Send data to the API
  // The data are the sums for those 30 seconds
  if (counterToSend == 30) {
    Serial.println("mean ECO2: ");
    Serial.println(sum_ECO2/30);
    sendData((sum_ECO2/30), (sum_TVOC/30));
    
    counterToSend = 0;
    sum_ECO2 = 0;
    sum_TVOC = 0;
  }

  // Every 12 hours
  // update the SGP30 baseline
  if (counterBaseline == (1440 * 30)) {
    counterBaseline = 0;

    if (! sgp.getIAQBaseline(&eCO2_base, &TVOC_base)) {
      Serial.println("Failed to get baseline readings");
      return;
    }
    
    Serial.print("****Baseline values: eCO2: 0x"); Serial.print(eCO2_base, HEX);
    Serial.print(" & TVOC: 0x"); Serial.println(TVOC_base, HEX);

    sgp.setIAQBaseline(eCO2_base, TVOC_base);
  }
}