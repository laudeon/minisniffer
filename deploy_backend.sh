#!/bin/bash

git branch deploy
git checkout deploy
git merge master

yarn build

git add .
git add --force ./lib
git commit -m "deploy"
git push heroku deploy:master --force
git checkout master
git branch -D deploy